package main

import "C"
import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"gopkg.in/ldap.v3"
	"io"
	log "github.com/sirupsen/logrus"
	"os"
)

var ldapUrl = os.Getenv("LDAP_URL")
var ldapUserPattern = os.Getenv("LDAP_USER_PATTERN")

func aesEncryptToBase64(key, text []byte) (string, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	paddedText, err := pkcs7Pad(text, block.BlockSize())
	if err != nil {
		return "", err
	}

	// CBC mode works on blocks so plaintexts may need to be padded to the
	// next whole block. For an example of such padding, see
	// https://tools.ietf.org/html/rfc5246#section-6.2.3.2. Here we'll
	// assume that the plaintext is already of the correct length.
	if len(paddedText)%aes.BlockSize != 0 {
		return "", errors.New("padded text is not a multiple of the block size")
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(paddedText))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(ciphertext[aes.BlockSize:], paddedText)

	return base64.StdEncoding.EncodeToString(ciphertext), nil
}

func pkcs7Pad(b []byte, blocksize int) ([]byte, error) {
	if blocksize <= 0 {
		return nil, errors.New("invalid block size")
	}
	if b == nil || len(b) == 0 {
		return nil, errors.New("invalid PKCS7 data")
	}
	n := blocksize - (len(b) % blocksize)
	pb := make([]byte, len(b)+n)
	copy(pb, b)
	copy(pb[len(b):], bytes.Repeat([]byte{byte(n)}, n))
	return pb, nil
}

//export go_authenticate
func go_authenticate(c_username *C.char, c_password *C.char) C.int {
	if len(ldapUrl) == 0 {
		log.Errorln("LDAP_URL environment variable is undefined.")
		return LkaTempfail
	}

	if len(ldapUserPattern) == 0 {
		log.Errorln("LDAP_USER_PATTERN environment variable is undefined.")
		return LkaTempfail
	}

	ldapRealUsername := fmt.Sprintf(ldapUserPattern, C.GoString(c_username))
	password := C.GoString(c_password)

	conn, err := ldap.Dial("tcp", ldapUrl)
	if err != nil {
		log.Errorf("Unable to connect to %s: %v", ldapUrl, err)
		return LkaTempfail
	}

	defer conn.Close()

	err = conn.Bind(ldapRealUsername, password)
	if err != nil {
		log.Errorf("Unable to authenticate to %s: %v", ldapUrl, err)
		return LkaPermfail
	}

	return LkaOk
}

//export go_log_debug
func go_log_debug(format string) {
	log.Debugf(format)
}

//export go_log_error
func go_log_error(format string) {
	log.Errorf(format)
}

// noop for libs
func main() {}
