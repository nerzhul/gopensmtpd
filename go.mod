module gitlab.com/nerzhul/gopemsmtpd

require (
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/ldap.v3 v3.0.3
)
